from thing import *
from box import *

def testRepr():
    t=Thing(5)
    t.setName('trousse')
    assert repr(t)=='trousse'
    assert t.hasName('trousse')
    assert not t.hasName('sarah')
    
def testFind():
    t=Thing(5)
    t.setName('objT')
    s=Thing(3)
    s.setName('objS')
    b=Box()
    b.open()
    #b.add('bonjour')
    b.add(t)
    b.add(s)
    print(b.find('objT'))
    assert b.find('objT') == t
    assert b.find('objS') == s
    assert b.find('autre') == None
    b.close()
    assert b.find('objS') == None
    
def testConstThing():
	t=Thing(5)
	assert t.getName() == None;

def testConstBox():
	b=Box(state=True)
	assert b.getCapacity()==None
	assert b.isOpen()
    
def testCreerListeBoites():
	l=CreateBoxList("mesBoites.yml")
	assert l[0].capacity==2
	assert l[0].isOpen==True
